#!/bin/bash

set -euo pipefail

ROOT_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
THEME_FOLDER="$ROOT_FOLDER/hack-dark"
ZIP_FILE="$ROOT_FOLDER/hack-dark.zip"

if [ -e "$ZIP_FILE" ]; then
  rm "$ZIP_FILE"
fi

pushd "$THEME_FOLDER" > /dev/null

zip "$ZIP_FILE" ./*

popd > /dev/null
